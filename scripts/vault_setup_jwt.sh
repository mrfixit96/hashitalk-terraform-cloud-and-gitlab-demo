#!/bin/bash

# **Important** VAULT_ADDR, VAULT_TOKEN, GOOGLE_PROJECT, and GOOGLE_CREDENTIALS_PATH environment variables should be set prior to running this script
# This script configures the GCP secrets Engine in Vault
# We assume the workspace name is gitlab-ci-gcp, please adjust this if needed

echo "Please ensure VAULT_ADDR, VAULT_TOKEN, TFC_TOKEN, GOOGLE_PROJECT and GOOGLE_CREDENTIALS_PATH environment variables are set."
echo "Optionally please set this variable to setup the JWT Auth method: GITLAB_PROJECT_ID"
./check_vault_vars.sh
if [ ! -f "vars_are_valid" ]; then
    echo "ERR: Failed to verify required Variables."
    exit 1
fi

echo "INFO: Checking connectivity to vault server: ${VAULT_ADDR}, and vault token"
curl -kv "${VAULT_ADDR}/v1/sys/health"
vault status
vault token lookup

echo "INFO: Creating the AppRole Auth Method"
vault auth enable approle
vault write auth/approle/role/gitlab \
    secret_id_ttl=24h \
    token_num_uses=50 \
    token_ttl=20m \
    token_max_ttl=30m \
    secret_id_num_uses=50 \
    policies="gitlab-runner"

vault read auth/approle/role/gitlab

# Use .data.role_id in role.json file as the ROLE_ID for Gitlab setup
echo "INFO: Role ID written to role.json file"
vault read -format=json auth/approle/role/gitlab/role-id > role.json
export ROLE_ID="$(cat role.json | jq -r .data.role_id )" && echo $ROLE_ID > roleid

# Use .data.secret_id in secretid.json file as the SECRET_ID for Gitlab credential
echo "INFO: Secret ID written to secret.json file"
vault write -format=json -f auth/approle/role/gitlab/secret-id > secretid.json
export SECRET_ID="$(cat secretid.json | jq -r .data.secret_id )" && echo $SECRET_ID > secretid

if [ ! -z "$GITLAB_PROJECT_ID" ]; then
  echo "INFO: Creating the JWT auth method"
  # Create the key/value secrets engine to store Terraform Cloud credentials
  vault auth enable jwt

  vault write auth/jwt/config \
    jwks_url="http://gitlab.com/oauth/discovery/keys" \
    bound_issuer="gitlab.com"

  echo "INFO: Creating a Role for GitLab under the jwt auth method"
  cat <<EOF > gitlab-runner-role.json
  {
    "role_type": "jwt",
    "policies": ["gitlab-runner"],
    "token_explicit_max_ttl": 300,
    "user_claim": "user_email",
    "bound_claims": {
    "project_id": "$GITLAB_PROJECT_ID",
    "ref": "jwt_auth",
    "ref_type": "branch"
    }
  }
EOF

vault write auth/jwt/role/gitlab @gitlab-runner-role.json

echo "NOTE: bound_claims is set to Gitlab project ID: $GITLAB_PROJECT_ID."

echo "INFO: Displaying the jwt auth method and role"
vault read auth/jwt/config
vault read auth/jwt/role/gitlab
  
else
  echo "INFO: Skipping the JWT auth method, please set a VAULT_TOKEN environment variable instead."
fi


echo "INFO: Create the kv2 secrets engine"
# Create the key/value secrets engine to store Terraform Cloud credentials
vault secrets enable -path=kv2 -version=2 kv
vault kv put kv2/tfc_token TFC_TOKEN=$TFC_TOKEN

echo "INFO: Creating secrets engine"
# Create the gcp secrets engine to issue credentials that are valid for five minutes  
vault secrets enable gcp
vault write gcp/config credentials=@${GOOGLE_CREDENTIALS_PATH}
vault secrets tune -default-lease-ttl=600s -max-lease-ttl=900s gcp

echo "INFO: Creating roleset binding"
# Create role set bindings for the dynamically generated credentials
vault write gcp/roleset/gitlab \
    project="${GOOGLE_PROJECT}" \
    secret_type="service_account_key"  \
    bindings=-<<EOF
resource "//cloudresourcemanager.googleapis.com/projects/${GOOGLE_PROJECT}" {
  roles = ["roles/editor"]
}
EOF

echo "INFO: Creating policy for GitLab Runner"
# Create a policy for GitLab runners
vault policy write gitlab-runner -<<EOH
path "gcp/key/gitlab" {
  capabilities = ["read"]
}
path "kv2/data/tfc_token" {
  capabilities = ["read"]
}
EOH

echo "INFO: Creating token for GitLab Runner"
# Create a token for the Runners with a TTL of 24 hours
vault token create -policy=gitlab-runner -ttl=24h

token=$(vault token create -format=json -policy=gitlab-runner -ttl=24h | jq -r .auth.client_token)
echo "INFO: Running test with token: $token"
VAULT_TOKEN=$token vault read -format=json gcp/key/gitlab && vault read -format=json kv2/data/tfc_token | grep TFC_TOKEN

echo "INFO: Testing AppRole Auth for Gitlab Runner"
token=$(vault write -format=json auth/approle/login role_id=$ROLE_ID secret_id=$SECRET_ID | jq -r .auth.client_token)
echo "INFO: Running test with token: $token"
VAULT_TOKEN=$token vault read -format=json gcp/key/gitlab && vault read -format=json kv2/data/tfc_token | grep TFC_TOKEN

echo "INFO: script completed. Please configure a VAULT_TOKEN variable in GitLab CI/CD Variables with value: $token"
echo "To test: VAULT_TOKEN=$token vault read -format=json gcp/key/gitlab && vault read -format=json kv2/data/tfc_token | grep TFC_TOKEN"
