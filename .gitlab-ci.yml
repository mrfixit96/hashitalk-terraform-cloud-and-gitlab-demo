# ------

# The stages contain one or more jobs executed by a GitLab Runner. Shared GitLab
# Runners are provided on GitLab.com for customers to run pipelines. For self
# managed GitLab, you can run a runner on anything that can run Golang.

stages:
  - create_workspace
  - setup_workspace
  - get_credentials
  - run
  - destroy


# ------

# The before_script runs before any job in any stage.

# If you will be writing / editing the CI YAML files then it's advised to bookmark
# this online reference: https://docs.gitlab.com/ee/ci/yaml/

# In the section below, three tasks are executed:
# 1) Login into Vault
# 2) Grab a Terraform token
# 3) Grab the Terraform workspace ID

before_script:
  - echo "CI_JOB_JWT is ${CI_JOB_JWT}"
  - echo "CI_PROJECT_ID is ${CI_PROJECT_ID}"
  - echo "CI_PROJECT_PATH is ${CI_PROJECT_PATH}"
  - echo "CI_PROJECT_NAMESPACE is ${CI_PROJECT_NAMESPACE}"
  - echo "CI_PIPELINE_ID is ${CI_PIPELINE_ID}"
  - echo "CI_JOB_ID is ${CI_JOB_ID}"
  - export VAULT_TOKEN="$(vault write -field=token auth/jwt/login role=gitlab jwt=${CI_JOB_JWT})"
  - export TFC_TOKEN="$(vault kv get -field=TFC_TOKEN ${TFC_TOKEN_CLI})"
  - >
    curl -s --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" "https://${TFC_ADDR}/api/v2/organizations/${TFC_ORG}/workspaces/${TFC_WORKSPACE}" | jq -r .data.id > workspace_id
  - export workspace_id=$(cat workspace_id)
  - echo "Workspace ID is ${workspace_id}"
  
# # ------
#
# # Get temporary GCP credential from Vault:
#
get_credentials:
  stage: get_credentials
  script:
  - echo "Checking if we can reach Vault @ ${VAULT_ADDR}"
  - vault status
  - echo "Getting new GCP credentials @ ${VAULT_ADDR}/v1/${SECRETS_PATH}"
  - vault kv get -field=private_key_data gcp/key/gitlab | base64 --decode > temp_creds
  - export GOOGLE_CREDENTIALS=$(tr '\n' ' ' < temp_creds | sed -e 's/\"/\\\\"/g' -e 's/\//\\\//g' -e 's/\\n/\\\\\\\\n/g')
  - rm -f temp_creds
  - sed -e "s/my-key/GOOGLE_CREDENTIALS/" -e "s/my-hcl/false/" -e "s/my-value/${GOOGLE_CREDENTIALS}/" -e "s/my-category/env/" -e "s/my-sensitive/true/" -e "s/my-workspace-id/${workspace_id}/" < api_templates/variable.json.template  > variable.json
  - >
    curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" --data @variable.json "https://${TFC_ADDR}/api/v2/vars"
  tags:
    - curl

# # ------
#
# # Either create or get the workspace ID if it already exists:

create_workspace:
  stage: create_workspace
  script:
  - sed "s/placeholder/${TFC_WORKSPACE}/" < api_templates/workspace.json.template > workspace.json
  - >
    curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" --request POST --data @workspace.json "https://${TFC_ADDR}/api/v2/organizations/${TFC_ORG}/workspaces" > workspace_result.txt
  - export workspace_id=$(cat workspace_result.txt | jq -r .data.id)
  - echo "Workspace ID is ${workspace_id}"
  - >
    export workspace_id=$(curl -s --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" "https://${TFC_ADDR}/api/v2/organizations/${TFC_ORG}/workspaces/${TFC_WORKSPACE}" | jq -r .data.id)
  - echo "Workspace ID is ${workspace_id}"
  tags:
    - curl

# # ------
#
# sets the Workspace Variables (Terraform and Environment variables) and
# uploads the terraform code (*.tf files)

workspace_vars:
  stage: setup_workspace
  script:
# Delete existing variables
  - >
    curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" "https://${TFC_ADDR}/api/v2/vars?filter%5Borganization%5D%5Bname%5D=${TFC_ORG}&filter%5Bworkspace%5D%5Bname%5D=${TFC_WORKSPACE}" > vars.json
  - x=$(cat vars.json | jq -r ".data[].id" | wc -l | awk '{print $1}')
  - >
    for (( i=0; i<$x; i++ ))
    do
       curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" --request DELETE https://${TFC_ADDR}/api/v2/vars/$(cat vars.json | jq -r ".data[$i].id")
    done
# Environment variables
  - sed -e "s/my-key/CONFIRM_DESTROY/" -e "s/my-hcl/false/" -e "s/my-value/1/" -e "s/my-category/env/" -e "s/my-sensitive/false/" -e "s/my-workspace-id/${workspace_id}/" < api_templates/variable.json.template  > variable0.json
# Terraform variables
  - sed -e "s/my-key/gcp_region/" -e "s/my-hcl/false/" -e "s/my-value/${gcp_region}/" -e "s/my-category/terraform/" -e "s/my-sensitive/false/" -e "s/my-workspace-id/${workspace_id}/" < api_templates/variable.json.template  > variable1.json
  - sed -e "s/my-key/gcp_project/" -e "s/my-hcl/false/" -e "s/my-value/${gcp_project}/" -e "s/my-category/terraform/" -e "s/my-sensitive/false/" -e "s/my-workspace-id/${workspace_id}/" < api_templates/variable.json.template  > variable2.json
  - if [ -z "$machine_type" ]; then machine_type="n1-standard-1"; fi
  - sed -e "s/my-key/machine_type/" -e "s/my-hcl/false/" -e "s/my-value/${machine_type}/" -e "s/my-category/terraform/" -e "s/my-sensitive/false/" -e "s/my-workspace-id/${workspace_id}/" < api_templates/variable.json.template  > variable3.json
  - >
    for (( i=0; i<4; i++ ))
    do
      curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" --data @variable$i.json "https://${TFC_ADDR}/api/v2/vars"
    done
  - >
    if [ ! -z "$GOOGLE_CREDENTIALS" ]; then
      sed -e "s/my-key/GOOGLE_CREDENTIALS/" -e "s/my-hcl/false/" -e "s/my-value/${GOOGLE_CREDENTIALS}/" -e "s/my-category/env/" -e "s/my-sensitive/true/" -e "s/my-workspace-id/${workspace_id}/" < api_templates/variable.json.template  > variable.json;
      curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" --data @variable.json "https://${TFC_ADDR}/api/v2/vars"; fi
  retry:
    max: 1
    when: script_failure
  tags:
    - curl

# # ------
# Ensure workspace is set with correct variables:
#

config_version:
  stage: setup_workspace
  script:
  - tar -cvf myconfig.tar *.tf scripts/webapp.sh
  - gzip myconfig.tar
  - >
    curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" --data @api_templates/configversion.json.template "https://${TFC_ADDR}/api/v2/workspaces/${workspace_id}/configuration-versions" > configuration_version.txt
  - cat configuration_version.txt
  - export config_version_id=$(cat configuration_version.txt | jq -r .data.id)
  - export upload_url=$(cat configuration_version.txt | jq -r '.["data"]["attributes"]["upload-url"]')
  - echo "Config Version ID is ${config_version_id}"
  - echo "Upload URL is ${upload_url}"
  - >
    curl --request PUT -F 'data=@myconfig.tar.gz' "${upload_url}"
  retry:
    max: 1
    when: script_failure
  tags:
    - curl
# ----

# Performs a terraform apply on Terraform Cloud. If Sentinel policies are broken
# then operator can override in that system

apply:
  stage: run
  script:
  - sed "s/my-workspace-id/${workspace_id}/" < api_templates/run.json.template  > run.json
  - cat run.json
  - >
    curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" --data @run.json https://${TFC_ADDR}/api/v2/runs > run_result.txt
  - cat run_result.txt
  - run_id=$(cat run_result.txt | jq -r .data.id)
  - echo "Run ID is ${run_id}"
  - >
    curl --header "Authorization: Bearer $TFC_TOKEN" --header "Content-Type: application/vnd.api+json" https://${TFC_ADDR}/api/v2/runs/${run_id} > result.txt
  - cat result.txt
  - result=$(cat result.txt | jq -r .data.attributes.status)
  - echo "Run result is ${result}. View this Run in TFC UI:"
  - echo "https://${TFC_ADDR}/app/${TFC_ORG}/workspaces/${TFC_WORKSPACE}/runs/${run_id}"
  tags:
    - curl

# ----
# Runs a terraform destroy in Terraform cloud:

destroy:
  stage: destroy
  when: manual
  script:
  - sed "s/my-workspace-id/${workspace_id}/" < api_templates/destroy.json.template  > run.json
  - cat run.json
  - >
    curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" --data @run.json https://${TFC_ADDR}/api/v2/runs > run_result.txt
  - cat run_result.txt
  - run_id=$(cat run_result.txt | jq -r .data.id)
  - echo "Run ID is ${run_id}"
  - >
    curl --header "Authorization: Bearer $TFC_TOKEN" --header "Content-Type: application/vnd.api+json" https://${TFC_ADDR}/api/v2/runs/${run_id} > result.txt
  - cat result.txt
  - result=$(cat result.txt | jq -r .data.attributes.status)
  - echo "Destroy Run result is ${result}. View this Run in TFC UI:"
  - echo "https://${TFC_ADDR}/app/${TFC_ORG}/workspaces/${TFC_WORKSPACE}/runs/${run_id}"
  tags:
    - curl

